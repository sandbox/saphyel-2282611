at_macky
========

This is sub-theme of the AdaptiveTheme base theme, you must first install the base theme first before enabling this sub-theme, and then enjoy it! Only uses a few pictures, this sub-theme only use 1 picture, everything else is done with CSS3. You can edit via theme settings.
